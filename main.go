// Fwrule is used to manage the outgoing firewall rules for the Quicksilver
// Iridium modem.
package main

import (
	"bufio"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"

	"bitbucket.org/uwaplmlf2/qs"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func loadRules(rdr io.Reader) ([]qs.FwRule, error) {
	rules := make([]qs.FwRule, 0)
	scanner := bufio.NewScanner(rdr)
	for scanner.Scan() {
		rule := qs.FwRule{}
		err := rule.ParseText(scanner.Bytes())
		if err != nil {
			return rules, err
		}
		rules = append(rules, rule)
	}

	return rules, scanner.Err()
}

func fmtRules(wtr io.Writer, rules []qs.FwRule) error {
	for _, rule := range rules {
		if b, err := rule.FmtText(); err != nil {
			return err
		} else {
			wtr.Write(b)
			wtr.Write([]byte("\n"))
		}
	}

	return nil
}

func main() {
	app := cli.NewApp()
	app.Name = "fwrule"
	app.Usage = "Manage outgoing firewall rules for the Quicksilver modem"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "url",
			Usage:  "Quicksilver Web API URL",
			Value:  "https://192.168.20.1/api",
			EnvVar: "QS_URL",
		},
		cli.StringFlag{
			Name:   "u",
			Usage:  "Quicksilver username:password",
			Value:  "",
			EnvVar: "QS_USER",
		},
		cli.StringFlag{
			Name:  "t",
			Usage: "Configuration type: runtime or startup",
			Value: "runtime",
		},
		cli.VersionFlag,
	}

	var cln *qs.Client
	app.Before = func(c *cli.Context) error {
		var err error
		cln, err = qs.NewClient(c.String("url"))
		if err != nil {
			return err
		}

		var ui *url.Userinfo
		if creds := c.String("u"); creds != "" {
			f := strings.Split(creds, ":")
			if len(f) < 2 {
				return fmt.Errorf("Invalid credentials: %q", creds)
			}
			ui = url.UserPassword(f[0], f[1])
		}

		return cln.Session(ui)
	}

	app.After = func(c *cli.Context) error {
		cln.CloseSession()
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:      "add",
			Usage:     "add one or more rules from a file",
			ArgsUsage: "RULEFILE",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				f, err := os.Open(c.Args().First())
				if err != nil {
					return cli.NewExitError(fmt.Errorf("File open failed: %v", err), 2)
				}
				defer f.Close()
				rules, err := loadRules(f)
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}
				cfgtype := qs.CfgType(c.GlobalString("t"))
				for _, rule := range rules {
					err = cln.AddFwRule(cfgtype, rule)
					if err != nil {
						return cli.NewExitError(fmt.Errorf("Cannot load rule %q: %v", rule, err), 2)
					}
				}
				return nil
			},
		},
		{
			Name:  "list",
			Usage: "list the current firewall rules",
			Action: func(c *cli.Context) error {
				rules, err := cln.FwRules(qs.CfgType(c.GlobalString("t")))
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}
				fmtRules(os.Stdout, rules)
				return nil
			},
		},
	}

	app.Run(os.Args)
}
