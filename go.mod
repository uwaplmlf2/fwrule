module bitbucket.org/uwaplmlf2/fwrule

go 1.17

replace bitbucket.org/uwaplmlf2/qs => ../qs

require (
	bitbucket.org/uwaplmlf2/qs v0.3.1
	github.com/urfave/cli v1.22.5
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
